public class Board
{
    private Square[][] tictactoeBoard;

    Board()
    {
        tictactoeBoard =  new Square[3][3];
        for(int row = 0; row<3; row++)
        {
            for(int col = 0; col<3; col++)
            {
                tictactoeBoard[row][col]  = Square.BLANK;
            }
        }
    }
    public String toString() 
    {
        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < tictactoeBoard.length; row++) 
        {
            for (int col = 0; col < tictactoeBoard[row].length; col++) 
            {
                sb.append(tictactoeBoard[row][col]).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
    public boolean placeToken(int row, int col, Square playerToken)
    {
        if(row>=0 && row<=2 && col >=0 && col <=2)
        {
            if(tictactoeBoard[row][col] == Square.BLANK)
            {
                tictactoeBoard[row][col] = playerToken;
                return true;
            }
        }
      return false;
    }
    public boolean checkIfFull()
    {
        for(int i = 0; i<2; i++)
        {
            for(int j = 0; j<2; j++)
            {
                if(tictactoeBoard[i][j] == Square.BLANK)
                {
                    return false;
                }
            }
        }
        return true;
    }
    public boolean checkifWinningHorizontal(Square playerToken)
    {
        for(Square[] row : tictactoeBoard)
        {
            int count = 0;
            for(Square element : row)
            {
                if(element.equals(playerToken))
                {
                    count++;
                }
                else
                {
                    count = 0;
                }
                if(count == 3)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean checkifWinningVertical(Square playerToken)
    {
        for(Square[] col : tictactoeBoard)
        {
            int count = 0;
            for(Square element : col)
            {
                if(element.equals(playerToken))
                {
                    count++;
                }
                else
                {
                    count = 0;
                }
                if (count==3)
                {
                    return true;
                }
            }
        }
        return false;
    }
}