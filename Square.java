public enum Square
{
    X("X"),
    O("O"),
    BLANK("_");
        
    private final String symbol;

    Square(String symbol)
    {
        this.symbol = symbol;
    }
    public String toString() 
    {
        return symbol;
    }
 }
