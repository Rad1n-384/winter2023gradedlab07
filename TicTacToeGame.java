import java.util.Scanner;
public class TicTacToeGame
{
 public static void main(String[] args)
 {
    Board board = new Board();
    boolean gameOver = false;
    int player = 1;
    Square playerToken = Square.X;
    
    System.out.println("Welcome to TicTacToe!!");
    System.out.println("Player 1's token: X");
    System.out.println("Player 2's token: O");
    Scanner scan = new Scanner(System.in);
    while(!gameOver)
    {
        System.out.println(board);
        if(player == 1)
        {
            playerToken = Square.X;
        }
        else
        {
            playerToken = Square.O;
        }
        
    System.out.println("Player " + player + ", please input which row you'd like to place your token");
    int row = scan.nextInt();
    System.out.println("Player " + player + ", please input which column you'd like to place your token");
    int col = scan.nextInt();
    while(board.placeToken(row,col,playerToken) == false)
    {
        System.out.println("Invalid input please try again");
        row = scan.nextInt();
        col = scan.nextInt();
    }
    if(board.checkIfFull())
    {
        System.out.println("It's a tie!");
    }
    else if(board.checkifWinningHorizontal(playerToken) == true)
    {
        System.out.println("Player "+ player+" is the winner!");
        gameOver = true;
    }
    else if(board.checkifWinningVertical(playerToken) == true)
    {
        System.out.println("Player "+ player+" is the winner!");
        gameOver = true;
    }
    else
    {
        player = player + 1;
        if(player > 2)
        {
            player = 1;
        }
    }
   }
 }
}